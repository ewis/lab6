
import numpy as np
from sklearn.cluster import KMeans
from sklearn import metrics
X = 10 * np.random.randn(100, 2) + 6
kmeans_model = KMeans(n_clusters=3, random_state=1).fit(X)
labels = kmeans_model.labels_
s = metrics.silhouette_score(X, labels, metric='euclidean')
print(s)

