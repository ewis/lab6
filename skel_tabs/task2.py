
import numpy as np
import matplotlib.pyplot as plt
import clustering


def run():
	## Step2. Generate the dataset to be clustered and print it (it should 100 rows and 2 columns)
	X = 10 * np.random.randn(100, 2) + 6
	print("data: ")
	print(X)

	print("k-means clustering")
	# clustering.plot_data(X)
	X, kmeans, silhouette_score, euclid_dist = clustering.clustering_kmeans(X, 3)

	clustering.plot_data_with_clusters(X, kmeans, True)

	## TODO: Task2. Compare the results (graphs) between 2, 3, 5 and 7 centroids. What do you observe?
	### Hint. How are the point organized in clusters?
	### Hint. How is the euclidian distance (used to measure the distance between points) influencing this?
