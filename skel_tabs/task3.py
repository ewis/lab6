
import numpy as np
import matplotlib.pyplot as plt
import clustering
import file


def run_countries_continents_case_study():
	X, head = file.read_csv_file('countries_continents.csv')

	print("data: ")
	print(X)
	print("k-means clustering")

	print("continents: ")
	continents = {e: i for i, e in enumerate(np.unique(X[:, 3]))}
	print(continents)

	# map data to numbers
	X[:, 3] = np.vectorize(continents.__getitem__)(X[:, 3])
	X = X[:, 1: np.size(X, 1)-1]

	X, kmeans, silhouette_score, euclid_dist = clustering.clustering_kmeans(X, len(continents))
	clustering.plot_data_with_clusters(X, kmeans, True, head[1], head[2], False)
	plt.axvline(x=np.min(X[:, 0] + (np.max(X[:, 0]) - np.min(X[:, 0])) / 2))
	plt.axhline(y=0)
	plt.show()

	## TODO: Task2. Compare the results (graphs) between 2, 3 and 4 centroids. What do you observe?
	### Hint. How are the point organized in clusters?
	### Hint. How would you label the clusters?
	### Hint. How is the euclidian distance (used to measure the distance between points) influencing this?


def run_countries_languages_case_study():
	X, head = file.read_csv_file('country_list_languages.csv')

	print("data: ")
	print(X)
	print("k-means clustering")

	# languages = {
	#     "English": 0,
	#     "French": 1,
	#     "German": 2
	# }
	languages = {e: i for i, e in enumerate(np.unique(X[:, 4]))}
	print(languages)
	# map data to numbers
	X[:, 3] = np.vectorize(languages.__getitem__)(X[:, 4])
	X = X[:, 1: np.size(X, 1)-1]

	X, kmeans, silhouette_score, euclid_dist = clustering.clustering_kmeans(X, 4)
	clustering.plot_data_with_clusters(X, kmeans, True, head[0], head[1], False)
	plt.axvline(x=np.min(X[:, 0] + (np.max(X[:, 0]) - np.min(X[:, 0])) / 2))
	plt.axhline(y=0)
	plt.show()

	## TODO: Task2. Compare the results (graphs) between 2, 3 and 4 centroids. What do you observe?
	### Hint. How are the point organized in clusters?
	### Hint. How would you label the clusters?
	### Hint. How is the euclidian distance (used to measure the distance between points) influencing this?


def run_market_segmentation_case_study():
	X, head = file.read_csv_file('market_segmentation_data.csv')

	print("data: ")
	print(X)
	print("k-means clustering")

	X, kmeans, silhouette_score, euclid_dist = clustering.clustering_kmeans(X, 4)
	clustering.plot_data_with_clusters(X, kmeans, True, head[0], head[1], False)
	plt.axvline(x=np.min(X[:, 0] + (np.max(X[:, 0]) - np.min(X[:, 0]))/2))
	plt.axhline(y=0)
	plt.show()

	## TODO: Task2. Compare the results (graphs) between 2, 3 and 4 centroids. What do you observe?
	### Hint. How are the point organized in clusters?
	### Hint. How would you label the clusters?
	### Hint. How is the euclidian distance (used to measure the distance between points) influencing this?


	optimal_number_of_clusters = 0
	silhouette_score_vect = []
	WCSS_vect = []
	max_silhouette_score = 0
	distances = []

	r = range(2, 10)
	for idx, i in enumerate(r):
		X, kmeans, silhouette_score, WCSS = clustering.clustering_kmeans(X, i)
		silhouette_score_vect.append(silhouette_score)

	WCSS_vect.append(WCSS)
	if silhouette_score > max_silhouette_score:
		max_silhouette_score = silhouette_score
		optimal_number_of_clusters = i

	print("optimal number of clusters: ", optimal_number_of_clusters)
	plt.subplot(211)
	plt.plot(r, silhouette_score_vect)
	plt.legend(["silhouette score"])
	plt.xticks(r)
	plt.subplot(212)
	plt.plot(r, WCSS_vect)
	plt.legend(["WCSS"])
	plt.xticks(r)
	plt.show()
