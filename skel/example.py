
import clustering
import numpy as np


def example_clustering():
    ## Step2. Generate the dataset to be clustered and print it (it should 100 rows and 2 columns)
    X = 10 * np.random.randn(100, 2) + 6
    print("data: ")
    print(X)

    print("k-means clustering")
    # clustering.plot_data(X)
    X, kmeans, _, _ = clustering.clustering_kmeans(X, 3)
    clustering.plot_data_with_clusters(X, kmeans, True)

    ## Task1. Modify the input parameters for Kmeans (See: https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html) to:
    ## -use more iterations
    ## -use as first iteration random instead of kmeans++
    ## Redo the graphics. What do you observe?

